import visa
import time

rm = visa.ResourceManager()
print(rm.list_resources())
print("")

my_intr = rm.open_resource(rm.list_resources()[0])
delay = .5

def show_vmax(source):
    my_intr.write(':MEASure:VMAX? '+source)
    time.sleep(delay)
    print("VoltageMax :    "+my_intr.read())
    
def show_vmin(source):
    my_intr.write(':MEASure:VMIN? '+source)
    time.sleep(delay)
    print("VoltageMin :    "+my_intr.read())
    
def show_frequency(source):
    my_intr.write(':MEASure:FREQuency? '+source)
    time.sleep(delay)
    print("Frequency  :    "+my_intr.read())

def show_vpp(source):
    my_intr.write(':MEASure:VPP? '+source)
    time.sleep(delay)
    print("VoltagePP  :    "+my_intr.read())


def show(ch):
    if(ch == 1):
        banner = 'CHANNEL 1'
        source = '[CHANnel1]'
    else:
        banner = 'CHANNEL 2'
        source = '[CHANnel2]'
        
    print('*****************************')
    print(banner+" MEASURE STATUS")
    print("")
    show_vmax(source)
    show_vmin(source)
    show_frequency(source)
    show_vpp(source)
    print('*****************************')
    print("")

def main():
    #for x in range(0,10):
    while(1):
        show(1)
        time.sleep(.1)
        my_intr.write(':MEASure:CLEar')
        show(2)
        
        if(rm.list_resources() == ()):
            break
            
main()